﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationManager : MonoBehaviour {
    #region Members
    public List<Agent> CurrentGeneration = null;

    #region Assets
    public GameObject AgentPrefab;
    public Sprite LeaderSprite;
    public Sprite RunnerUpSprite;
    public Sprite NormalSprite;
    #endregion

    #region Simulation Values
    public int AgentsActive = 0;
    public bool SimulationActive = false;
    public int GenerationCount = 0;
    #endregion

    #region UI
    public Camera Leader_AllTime, Leader_Alive, RunnerUp_AllTime;
    public LineRenderer Leader_AllTime_Line, Leader_Alive_Line, RunnerUp_AllTime_Line;
    public Transform LeaderView, RunnerUpView, LeaderAliveView;
    public UnityEngine.UI.Text GenerationText;
    public UnityEngine.UI.Slider TimeSlider;
    #endregion
    #endregion

    #region Event Handlers
    #region UI Event Handlers
    public void SetTimeScale()
    {
        Time.timeScale = TimeSlider.value;
    }
    #endregion

    #region Unity Event Handlers
    private void Start()
    {
        foreach (GameObject go in FindObjectsOfType<GameObject>()) // disable renderers all checkpoints   
            if (go.GetComponent<Checkpoint>() != null)
                go.GetComponent<SpriteRenderer>().enabled = false;
        string[] SimParams = System.IO.File.ReadAllLines(System.IO.Directory.GetCurrentDirectory() + @"\settings.config");
        SimulationParameters.MaxAcceleration = float.Parse(SimParams[0].Split(null)[1]);
        SimulationParameters.MaxTurnPower = float.Parse(SimParams[1].Split(null)[1]);
        SimulationParameters.MaxSpeed = float.Parse(SimParams[2].Split(null)[1]);
        SimulationParameters.Timeout = float.Parse(SimParams[3].Split(null)[1]);
        SimulationParameters.GenerationSize = int.Parse(SimParams[4].Split(null)[1]);
        SimulationParameters.MutationChance = float.Parse(SimParams[5].Split(null)[1]);
        Invoke("FirstEvaluation", 1.0f); // give time for checkpoints to initialize
        System.IO.File.Delete(System.IO.Directory.GetCurrentDirectory() + @"\CurrentSimulationWinners.json");
    }

    private void Update()
    {
        SetTimeScale();
        if (SimulationActive) // update sprites to reflect current winner
        {
            CurrentGeneration.Sort();
            double maxFitness = CurrentGeneration[0].Fitness;
            int findingplace = 1;
            foreach(Agent agent in CurrentGeneration)
            {
                if(findingplace == 3)
                {
                    agent.GetComponent<SpriteRenderer>().sprite = NormalSprite;
                    if (!agent.IsAlive)
                    {
                        agent.GetComponent<SpriteRenderer>().color = new Color(0,0,0, 0.3f);
                    }
                }
                if(findingplace == 2)
                {
                    agent.GetComponent<SpriteRenderer>().sprite = RunnerUpSprite;
                    findingplace++;
                    if (!agent.IsAlive)
                    {
                        agent.GetComponent<SpriteRenderer>().color = new Color(agent.GetComponent<SpriteRenderer>().color.r, agent.GetComponent<SpriteRenderer>().color.g, agent.GetComponent<SpriteRenderer>().color.b, 0.3f);
                    }

                    Vector3 pos = agent.transform.position;
                    pos.z = -10;
                    RunnerUp_AllTime.transform.position = Vector3.Lerp(RunnerUp_AllTime.transform.position, pos, Time.deltaTime * 10);

                    Vector3 pos2 = agent.transform.position;
                    pos2.z = 0;
                    Vector3 pos3 = RunnerUpView.position;
                    pos3.z = 0;
                    RunnerUp_AllTime_Line.SetPositions(new Vector3[2] { pos2, pos3 });
                }
                if(findingplace == 1)
                {
                    agent.GetComponent<SpriteRenderer>().sprite = LeaderSprite;
                    findingplace++;
                    if (!agent.IsAlive)
                    {
                        agent.GetComponent<SpriteRenderer>().color = new Color(agent.GetComponent<SpriteRenderer>().color.r, agent.GetComponent<SpriteRenderer>().color.g, agent.GetComponent<SpriteRenderer>().color.b, 0.3f);
                        Leader_AllTime.transform.position = agent.transform.position;
                    }

                    Vector3 pos = agent.transform.position;
                    pos.z = -10;
                    Leader_AllTime.transform.position = Vector3.Lerp(Leader_AllTime.transform.position,pos,Time.deltaTime*10);

                    Vector3 pos2 = agent.transform.position;
                    pos2.z = 0;
                    Vector3 pos3 = LeaderView.position;
                    pos3.z = 0;
                    Leader_AllTime_Line.SetPositions(new Vector3[2] { pos2, pos3 });
                }
            }
            foreach(Agent agent in CurrentGeneration)
            {
                if (agent.IsAlive)
                {
                    Vector3 pos = agent.transform.position;
                    pos.z = -10;
                    Leader_Alive.transform.position = Vector3.Lerp(Leader_Alive.transform.position, pos, Time.deltaTime * 10);

                    Vector3 pos2 = agent.transform.position;
                    pos2.z = 0;
                    Vector3 pos3 = LeaderAliveView.position;
                    pos3.z = 0;
                    Leader_Alive_Line.SetPositions(new Vector3[2] { pos2, pos3 });
                    break;
                }
            }
        }
    }
    #endregion

    // called when an agent is removed from the simulation
    private void OnAgentRemoved(Agent obj)
    {
        AgentsActive--;
        if (AgentsActive == 0) // end of this generation
        {
            SimulationActive = false;
            FinishEvaluation();
        }
    }
    #endregion

    private void GeneratePopulation(List<NeuralNetwork> neuralnets = null)
    {
        // find the start checkpoint
        GameObject StartCheckpoint = null;
        GameObject NextCheckpoint = null; 
        foreach(GameObject go in FindObjectsOfType<GameObject>())
        {
            if(go.name == "Checkpoint_Start")
            {
                StartCheckpoint = go;
                
                break;
            }
        }

        // set the next checkpoint and find the initial position and rotations the car should have
        NextCheckpoint = StartCheckpoint.GetComponent<Checkpoint>().Next_Checkpoint;
        Vector3 rot = (NextCheckpoint.transform.position - StartCheckpoint.transform.position);
        Vector2 rot2 = new Vector2(rot.x, rot.y);
        float rotationAngle = Vector2.SignedAngle(Vector2.up, rot2);

        // remove the previous generation
        foreach(Agent agent in CurrentGeneration)
        {
            Destroy(agent.gameObject);
        }
        CurrentGeneration.Clear();
        CurrentGeneration = new List<Agent>();

        // create gameobjects
        for (int i = 0; i < SimulationParameters.GenerationSize; i++)
        {
            GameObject GeneratedAgent = Instantiate(AgentPrefab, new Vector3(StartCheckpoint.transform.position.x, StartCheckpoint.transform.position.y, 0), Quaternion.identity);
            GeneratedAgent.name += " " + i.ToString();
            GeneratedAgent.GetComponent<Rigidbody2D>().rotation = rotationAngle;
            CurrentGeneration.Add(GeneratedAgent.GetComponent<Agent>());
        }

        // initialise agents
        if (neuralnets == null) // first generation
        {
            
            foreach(Agent agent in CurrentGeneration)
            {
                agent.AgentRemoved += OnAgentRemoved; 
                agent.GenerateANN(new int[5] { 5, 3, 3, 3, 2 }); // generate neural net
                AgentsActive++;
            }
            SimulationActive = true;
        }
        else
        {
            // create new generation of agents using nets provided
            for (int i = 0; i < SimulationParameters.GenerationSize; i++)
            {
                CurrentGeneration[i].AgentRemoved += OnAgentRemoved;
                CurrentGeneration[i].ANN = neuralnets[i]; // set neural net specified in the function params
                AgentsActive++;
                
            }
            SimulationActive = true;
        }
    }

    // called when simulation is finished, calls genetic operators to be run on the generation
    public void FinishEvaluation()
    {
        // updates the generation label on the UI
        GenerationCount++;
        GenerationText.text = "Generation " + GenerationCount.ToString();
        
        List<NeuralNetwork> newGen = new List<NeuralNetwork>();
        List<NeuralNetwork> curGen = new List<NeuralNetwork>();

        // sort by fitness
        CurrentGeneration.Sort();

        // remove the event handlers
        foreach(Agent agent in CurrentGeneration)
        {
            curGen.Add(new NeuralNetwork(agent.ANN.NetTopology,agent.ANN.Weights));
            agent.AgentRemoved -= OnAgentRemoved;
        }

        // uses genetic operators to evolve the neural network
        newGen = GeneticAlgorithm.Evolve(curGen);

        // begins another simulation with the new generation
        StartEvaluation(newGen);
    }

    // starts evaluation with specified neural networks (randomly generated if none specified)
    public void StartEvaluation(List<NeuralNetwork> neuralnets = null) 
    {
        GeneratePopulation(neuralnets);
    }

    // special case for Unity "Invoke" as it cannot invoke functions with parameters
    public void FirstEvaluation()
    {
        List<NeuralNetwork> nets = new List<NeuralNetwork>();
        if (System.IO.File.Exists("InitialNeuralNet.json"))
        {
            string jsontext = "";
            try
            {
                jsontext = (System.IO.File.ReadAllLines(System.IO.Directory.GetCurrentDirectory() + @"\InitialNeuralNet.json")[0].Length > 1) ? System.IO.File.ReadAllLines(System.IO.Directory.GetCurrentDirectory() + @"\InitialNeuralNet.json")[0] : null;
                if (string.IsNullOrEmpty(jsontext))
                {
                    StartEvaluation();
                    return;
                }
            }
            catch{
                StartEvaluation();
                return;
            }
            
            
            for (int i = 0; i < 50; i++) nets.Add(JsonUtility.FromJson<NeuralNetwork>(jsontext));
            StartEvaluation(nets);
        }
        else
        {

            StartEvaluation();
        } 
    }
}
