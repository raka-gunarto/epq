﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour, IComparable<Agent> {

    #region Members
    #region Agent Behaviour
    public CarController Controller;
    [System.NonSerialized] // this is to prevent Unity from initialising the ANN
    public NeuralNetwork ANN = null;
    public Sensor[] Sensors;
    #endregion

    #region Simulation
    public bool IsAlive = true;
    public Checkpoint CurrentCP; // stores the last CP
    public Checkpoint NextCP;
    public int CPPassed = 0;
    public float TimePassed = 0;
    public double Fitness = 0;
    private int CPCount;
    #endregion

    #region Relative Direction Vectors
    public Vector2 ForwardVec
    {
        get
        {
            foreach (Sensor s in Sensors)
            {
                if (s.FacingDir == Sensor.FACING.FORWARD)
                    return (s.transform.position - this.transform.position).normalized;
            }
            return new Vector2(-1, -1);
        }
    }

    public Vector2 LeftVec
    {
        get
        {
            foreach (Sensor s in Sensors)
            {
                if (s.FacingDir == Sensor.FACING.LEFT)
                    return (s.transform.position - this.transform.position).normalized;
            }
            return new Vector2(-1, -1);
        }
    }

    public Vector2 RightVec
    {
        get
        {
            foreach (Sensor s in Sensors)
            {
                if (s.FacingDir == Sensor.FACING.RIGHT)
                    return (s.transform.position - this.transform.position).normalized;
            }
            return new Vector2(-1, -1);
        }
    }
    #endregion
    #endregion

    #region Events
    public event Action<Agent> AgentRemoved; // event when agent dies/is removed from the simulation
    #endregion

    #region IComparable
    public int CompareTo(Agent other)
    {
        if (this.Fitness > other.Fitness)
            return -1; // precedes
        else if (this.Fitness < other.Fitness)
            return 1; // follows
        else
            return 0; // unlikely, but same fitness so no difference
    }
    #endregion

    #region Unity Event Handlers
    #region Start
    void Start() {
        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
        Controller = this.GetComponent<CarController>(); // gets the car controller script instance
        Sensors = this.GetComponentsInChildren<Sensor>(); // gets all the sensors
        CPCount = 0;
        foreach (GameObject go in FindObjectsOfType<GameObject>()) // count checkpoints
        {
            if (go.name.Contains("Checkpoint_")) CPCount++;
        }
        foreach (GameObject go in FindObjectsOfType<GameObject>()) // get starting checkpoint
        {
            if (go.GetComponent<Checkpoint>() != null && go.name == "Checkpoint_Start")
            {
                CurrentCP = go.GetComponent<Checkpoint>();
                NextCP = go.GetComponent<Checkpoint>();
            }
        }
    }
    #endregion

    #region Updates
    void Update()
    {
        if (ANN != null && IsAlive) // check alive or not
        {
            // check timeout
            TimePassed += Time.deltaTime;
            if (TimePassed > SimulationParameters.Timeout)
                Die();

            if (NextCP.gameObject.name == CurrentCP.gameObject.name && NextCP.gameObject.name != "Checkpoint_Start")
            {
                print("Passed " + NextCP.gameObject.name + " " + CurrentCP.gameObject.name);
                
                Fitness = 1.1;
            }
            else
            {
                float distanceToNextCP = Vector2.Distance(this.transform.position, NextCP.transform.position);
                float distanceBetweenCPs = Vector2.Distance(CurrentCP.transform.position, NextCP.transform.position);
                if (distanceToNextCP > distanceBetweenCPs)
                    distanceToNextCP = distanceBetweenCPs - distanceToNextCP;

                Fitness = ((float)CPPassed / (float)CPCount) + ((distanceToNextCP / distanceBetweenCPs) * (1f / (float)CPCount));
                if (Double.IsNaN(Fitness))
                {
                    Fitness = 0;
                }
            }
        }
    }
    void FixedUpdate()
    {
        if (ANN != null && IsAlive) // check alive or not
        {
            // get inputs and process
            double[] inputs = GetInputs();
            double[] outputs = ANN.Evaluate(inputs);

            // send outputs to carcontroller and move
            Controller.Move(outputs);
        }
    }
    #endregion

    #region Collisions
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Checkpoint>() != null) // we hit a checkpoint
        {
            if (collision.gameObject.name == "Checkpoint_Finish")
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.IO.Directory.GetCurrentDirectory() + @"\CurrentSimulationWinners.json",true))
                    file.WriteLine(JsonUtility.ToJson(ANN));
            
                Fitness = 1.1;
                Die();
                return;
            }
        }
        if (collision.gameObject.name == NextCP.gameObject.name) // we hit the next checkpoint
        {
            CurrentCP = NextCP;
            NextCP = NextCP.Next_Checkpoint.GetComponent<Checkpoint>(); // get the next checkpoint in the linked list
            CPPassed++;
            TimePassed = 0.0f;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {   
        Die(); // we hit a wall 
    }
    #endregion
    #endregion

    // removes the agent from the current simulation
    private void Die() 
    {
        if (!IsAlive)
            return; // ignore
        IsAlive = false;
        this.GetComponent<Rigidbody2D>().simulated = false;
        // bring car to halt
        Controller.Stop();

        AgentRemoved(this); // broadcast the AgentRemoved event
    }

    // generates a random ANN for the agent
    public void GenerateANN(int[] topology, List<double[,]> LayerWeights = null)
    {
        ANN = new NeuralNetwork(topology, LayerWeights);
    }

    // returns the raycast outputs from the 5 sensors
    private double[] GetInputs()
    {
        double[] inputs = new double[5];
        int i = 0;
        foreach(Sensor sensor in Sensors)
        {
            Vector2 origin = new Vector2(sensor.transform.position.x, sensor.transform.position.y);
            Vector2 direction = new Vector2((sensor.transform.position.x - this.transform.position.x), (sensor.transform.position.y - this.transform.position.y));
            RaycastHit2D rayHit = Physics2D.Raycast(origin, direction, 1.0f, LayerMask.GetMask("Wall"));

            inputs[i++] = (rayHit.distance != 0) ? rayHit.distance : 1.0f; // raycast collisions
        }
        return inputs;
    }


}
