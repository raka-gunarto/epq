﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour {
    public enum FACING { FORWARD, FORWARD_LEFT, FORWARD_RIGHT, LEFT, RIGHT};
    // Use this for initialization
    public FACING FacingDir;
}
