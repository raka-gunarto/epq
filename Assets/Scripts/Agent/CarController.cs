﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {
    public double CurAcceleration;
    public double CurTurningPower;

    Rigidbody2D carbody;
    Agent caragent;

    public float vX, vY;

    private void Start()
    {
        carbody = this.GetComponent<Rigidbody2D>();
        caragent = this.GetComponent<Agent>();
    } 

    public void Stop()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.GetComponent<Rigidbody2D>().simulated = false;
    }

    public void Move(double[] inputs)
    {
            CurAcceleration = inputs[0] * SimulationParameters.MaxAcceleration;
        CurTurningPower = inputs[1] * SimulationParameters.MaxTurnPower;
        CurAcceleration = System.Math.Abs(CurAcceleration);
        if (carbody.velocity.magnitude < SimulationParameters.MaxSpeed)
        {
            carbody.velocity += (caragent.ForwardVec.normalized * (float)CurAcceleration * Time.fixedDeltaTime) - (carbody.velocity * 0.01f); 
            carbody.velocity = Quaternion.AngleAxis((float)CurTurningPower, new Vector3(0, 0, 1)) * carbody.velocity;
        }
    }

    private void FixedUpdate()
    {
        vX = carbody.velocity.x;
        vY = carbody.velocity.y;
        if (caragent.ANN != null && caragent.IsAlive) 
            carbody.rotation = Vector2.SignedAngle(Vector2.up, this.GetComponent<Rigidbody2D>().velocity);
    }
}
