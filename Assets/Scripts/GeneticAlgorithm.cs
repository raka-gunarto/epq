﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GeneticAlgorithm{
    public static List<NeuralNetwork> Crossover(List<NeuralNetwork> currentGen, List<NeuralNetwork> newGen)
    {
        // keep best two
        newGen.Add(new NeuralNetwork(currentGen[0].NetTopology, currentGen[0].Weights));
        newGen.Add(new NeuralNetwork(currentGen[1].NetTopology, currentGen[1].Weights));

        // crossover
        int[] topology = currentGen[0].NetTopology;
        int layers = currentGen[0].Layers.Length;

        while(newGen.Count < 50)
        {
            List<double[]> LayerWeights = new List<double[]>(layers);

            //--Keep picking two random neural networks until two different ones are picked
            int neural1idx = Random.Range(0, currentGen.Count - 1);
            int neural2idx = Random.Range(0, currentGen.Count - 1);
            while (neural1idx == neural2idx)
            {
                neural1idx = Random.Range(0, currentGen.Count - 1);
                neural2idx = Random.Range(0, currentGen.Count - 1);
            }
            //--

            // loop through all layers
            for (int currentLayer = 0; currentLayer < layers; currentLayer++)
            {           
                NeuralLayer nn_layer1 = currentGen[neural1idx].Layers[currentLayer];
                NeuralLayer nn_layer2 = currentGen[neural2idx].Layers[currentLayer];

                LayerWeights.Add(new double[nn_layer1.ConnectionWeightsLinear.Length]);

                // loop through all the weights and randomly choose weights from the parent
                for (int curWeight = 0; curWeight < nn_layer1.ConnectionWeightsLinear.Length; curWeight++)
                {
                    LayerWeights[currentLayer][curWeight] = (Random.Range(0, 100) < 50) ? nn_layer1.ConnectionWeightsLinear[curWeight] : nn_layer2.ConnectionWeightsLinear[curWeight]; // 50:50 chance of which parent to use
                }
            }

            // adds the offspring to the new generation
            newGen.Add(new NeuralNetwork(topology, null, LayerWeights));
        }

        return newGen;
    }

    public static List<NeuralNetwork> Mutate(List<NeuralNetwork> newGen)
    {
        // loop through all neurons 
        foreach(NeuralNetwork nn in newGen)
        {
            foreach(NeuralLayer nn_layer in nn.Layers)
            {
                for(int i = 0; i < nn_layer.Neurons; i++)
                {
                    for(int j = 0; j < nn_layer.OutputNeurons; j++)
                    {
                        if(Random.RandomRange(0,100) < (SimulationParameters.MutationChance * 100)) // calculate random number and compare with mutation rate set in SimulationParameters
                        {
                            nn_layer.ConnectionWeights[i, j] = Random.Range(-1f, 1f); // mutate neuron (generate random value) 
                        }
                    }
                }
            }
        }
        return newGen;
    }

	public static List<NeuralNetwork> Evolve(List<NeuralNetwork> currentGen)
    {
        List<NeuralNetwork> newGeneration = new List<NeuralNetwork>();

        // selection (prune worst 90%)
        currentGen.RemoveRange(5, 45); 

        // crossover
        newGeneration = Crossover(currentGen, newGeneration);

        // mutate
        newGeneration = Mutate(newGeneration);

        return newGeneration;
    }
}
