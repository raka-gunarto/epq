﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationParameters : MonoBehaviour { // stores global simulation variables
    
    public static float MaxAcceleration = 2.0f;
    public static float MaxTurnPower = 20.0f;
    public static float MaxSpeed = 10.0f;
    public static float Timeout = 10.0f; // 10 seconds timeout
    public static int GenerationSize = 50;
    public static float MutationChance = 0.05f;
}
