﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class NeuralLayer {
    #region Members
    public double[,] ConnectionWeights = null;
    public int Neurons;
    public int OutputNeurons;
    public double[] ConnectionWeightsLinear = null;
    public bool created = false;
    #endregion

    #region Constructors
    public NeuralLayer(int neurons, int outputNeurons, double[,] ConnectionWeights = null, double[] ConnectionWeightsLinear = null) // constructor
    {
        this.Neurons = neurons;
        this.OutputNeurons = outputNeurons;

        if (ConnectionWeights != null) // init from connection weights provided
        {
            this.ConnectionWeights = ConnectionWeights;
            int linearcounter = 0;
            this.ConnectionWeightsLinear = new double[Neurons * OutputNeurons];
            for (int i = 0; i < Neurons; i++)
            {
                for (int j = 0; j < OutputNeurons; j++)
                {
                    this.ConnectionWeightsLinear[linearcounter++] = this.ConnectionWeights[i, j];
                }
            }
        }
        else if(ConnectionWeightsLinear != null) // init from 1D array of connection weights
        {
            this.ConnectionWeightsLinear = ConnectionWeightsLinear;
            InitFromLinearData();
        }
        else
        {
            this.ConnectionWeights = new double[Neurons, OutputNeurons]; // 2 dimensional array, 1 array of weights for each neuron
            this.ConnectionWeightsLinear = new double[Neurons * OutputNeurons];
            InitializeLayer();
        }
    }
    #endregion

    #region Layer Initialisation 
    public void InitFromLinearData() // set the weights from the 1D array provided
    {
        this.ConnectionWeights = new double[Neurons, OutputNeurons];
        int linearcounter = 0;
        for (int i = 0; i < Neurons; i++)
        {
            for (int j = 0; j < OutputNeurons; j++)
            {
                ConnectionWeights[i, j] = ConnectionWeightsLinear[linearcounter++]; // get weight from the list
            }
        }
    }

    private void InitializeLayer() // set random weights
    {
        int linearcounter = 0;
        for(int i = 0; i < Neurons; i++)
        {
            for(int j = 0; j < OutputNeurons; j++)
            {
                ConnectionWeights[i, j] = UnityEngine.Random.Range(-1f, 1f); // random weight between -1 and 1
                ConnectionWeightsLinear[linearcounter++] = ConnectionWeights[i, j];
            }
        }
    }
    #endregion

    // returns the summation of the multiplied inputs (multiplied with the set weights) with the activation funtion applied
    public double[] Evaluate(double[] inputs)
    {
        if (ConnectionWeights == null && ConnectionWeightsLinear != null) InitFromLinearData(); // generated from linear data from crossover

        double[] outputs = new double[OutputNeurons]; // set size of output to number of outputs this layer has
        for (int i = 0; i < Neurons; i++) // use weights to calculate output
        {
            for (int j = 0; j < OutputNeurons; j++)
            {
                outputs[j] += inputs[i] * ConnectionWeights[i, j];
            }
        }

        for (int i = 0; i < OutputNeurons; i++) // use tanh activation function
            outputs[i] = Math.Tanh(outputs[i]);

        return outputs;
    }
}
