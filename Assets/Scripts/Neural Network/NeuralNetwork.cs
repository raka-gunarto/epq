﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NeuralNetwork {
    #region Members
    public NeuralLayer[] Layers;
    public int[] NetTopology;
    public List<double[,]> Weights
    {
        get
        {
            List<double[,]> weights = new List<double[,]>();
            foreach(NeuralLayer nn_layer in Layers)
            {
                weights.Add(nn_layer.ConnectionWeights);
            }
            return weights;
        }
    }
    #endregion

    #region Constructors
    public NeuralNetwork(int[] NetTopology, List<double[,]> LayerWeights = null, List<double[]> LayerWeightsLinear = null)
    {   
        this.NetTopology = NetTopology;
        Layers = new NeuralLayer[NetTopology.GetLength(0) - 1]; // last layer doesn't have outputs, so -1 
        for(int i = 0; i < NetTopology.GetLength(0) - 1; i++)
        {
            if(LayerWeights != null)
                Layers[i] = new NeuralLayer(NetTopology[i], NetTopology[i + 1], LayerWeights[i], null); // set weights if specified
            else if (LayerWeightsLinear != null)
                Layers[i] = new NeuralLayer(NetTopology[i], NetTopology[i + 1], null, LayerWeightsLinear[i]); // set weights if specified
            else
                Layers[i] = new NeuralLayer(NetTopology[i], NetTopology[i + 1], null, null); // set weights if specified
        }
    }
    #endregion

    // evaluates inputs by propagating them through all the layers
    public double[] Evaluate(double[] inputs)
    {
        foreach(NeuralLayer layer in Layers)
        {
            inputs = layer.Evaluate(inputs); // feed forward, last output to next layer
        }
        return inputs; // this now contains the final output of the neural network  
    }
}
