﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {
    public GameObject Next_Checkpoint = null;

    void Start() // Function called when script is first executed
    {
        //--Special Cases
        if(gameObject.name == "Checkpoint_Start") // start CP has no number, special case
        {
            foreach(GameObject go in FindObjectsOfType<GameObject>())
            {
                if(go.GetComponent<Checkpoint>() != null && go.name == "Checkpoint_ (0)") {
                    Next_Checkpoint = go;
                    return;
                }
            }
        }
        if (gameObject.name == "Checkpoint_Finish") // no CP after finish
        {
            Next_Checkpoint = this.GetComponent<Checkpoint>().gameObject;
            return;
        } 
        //--

        //--Find next CP using number
        string numStr = gameObject.name.Substring(12);
        numStr = numStr.Substring(1, numStr.Length - 2);
        int num = int.Parse(numStr);
        foreach(GameObject go in FindObjectsOfType<GameObject>())
        {
            if (go.GetComponent<Checkpoint>() != null && go.name == string.Format("Checkpoint_ ({0})", (num + 1).ToString()))
            {
                Next_Checkpoint = go;
                return;
            }
        }
        //--

        // we couldn't find one, so it must be second last!
        foreach (GameObject go in FindObjectsOfType<GameObject>())
        {
            if (go.GetComponent<Checkpoint>() != null && go.name == "Checkpoint_Finish")
            {
                Next_Checkpoint = go;
                return;
            }
        }
    }
}
